import pytest
from matplotlib import pyplot as plt

import rna.plotting
from rna.plotting.backends.matplotlib import PairGrid


@pytest.mark.parametrize(
    "kwargs",
    [
        dict(x_vars=["a", "b"], y_vars=["c", "d"]),
        dict(x_vars=["a", "b"], y_vars=["a", "b"], diag=False, corner=False),
    ],
)
def test_pair_plot(kwargs):
    pg = plt.figure(FigureClass=PairGrid, **kwargs)

    def plot_func(ax, x_var, y_var, index):
        ax.scatter(index[0], index[1])

    pg.map(plot_func)
    rna.plotting.show()


if __name__ == "__main__":
    test_pair_plot(dict(x_vars=["a", "b"], y_vars=["a", "d"], diag=False, corner=True))
