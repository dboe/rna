"""
Testing colors
"""
import unittest
import rna.plotting


class ColorConversionTest(unittest.TestCase):
    """
    Color test
    """

    def setUp(self):
        self.inst = dict(vmin=0, vmax=1, cmap="rainbow")

    def test_format_colors(self):
        """
        Test color formatting in loop
        """
        color_start = "r"
        color_start = rna.plotting.ApiBackend.format_colors(
            self.inst, color_start, fmt="rgba", length=1
        )
        color = color_start
        for fmt in ("norm", "rgb", "hex", "rgba"):
            color = rna.plotting.ApiBackend.format_colors(
                self.inst, color, fmt=fmt, length=1
            )
            self.assertEqual(rna.plotting.colors.color_fmt(color), fmt)
        self.assertEqual(color, color_start)


if __name__ == "__main__":
    unittest.main()
