import rna


def hash_function(fun):
    return fun.__module__ + "." + fun.__qualname__


class function_cache(rna.pattern.decorator.Decorator):
    """Define the expected precision for your evaluation.
    Used for testing purposes.
    """

    CACHE = {}

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs

    @property
    def wrapped(self):
        if self._wrapped is None:
            # First time request
            func_key = hash_function(self._func)
            self.__class__.CACHE[func_key] = (self._args, self._kwargs)
        return super().wrapped

    def _wrap(self, this, func, *args, **kwargs):
        return func(self, this, func, *args, **kwargs)


class Example:
    @function_cache(42, a=42)
    def compute(self, *args, **kwargs):
        return args, kwargs
