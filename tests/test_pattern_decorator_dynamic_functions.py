"""
Testing the base decorator implementation
"""
import unittest
import rna.pattern.decorator
import rna.pattern.backend


# pylint:disable=too-few-public-methods,invalid-name
class node(rna.pattern.decorator.Decorator):
    """
    Example decorator on how to build the above easier.
    """

    def _wrap(self, this, func, *args, **kwargs):
        return func(*args, **kwargs)


# pylint:disable=too-few-public-methods,invalid-name
class dependencies(rna.pattern.decorator.Decorator):
    """
    Example decorator on how to build the above easier.
    """

    def __init__(self, value, foo):
        assert value == 42
        assert foo == 84

    def _wrap(self, this, func, *args, **kwargs):
        assert this is None or isinstance(this, object)
        return func(*args, **kwargs)


class ExampleClass:
    """Methods and static methods that return (self,) args and kwargs"""

    def __init__(self):
        self._function = lambda state: state

    @property
    def function(self):
        return self._function

    @function.setter
    def function(self, function):
        self._function = function

    @node
    @dependencies(42, 84)
    def compute(self, *args):
        """
        My meth
        """
        return self.function(*args)


class DecoTest(unittest.TestCase):
    """
    Color test
    """

    def setUp(self):
        self.inst = ExampleClass()

    def test_noderation(self):
        """
        Test function and method noderation
        """
        self.assertEqual(self.inst.compute(1), 1)
        self.inst.function = lambda state: state * 2
        self.assertEqual(self.inst.compute(1), 2)
