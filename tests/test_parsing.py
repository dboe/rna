import argparse
import rna.parsing
import logging


def test_log_parser(tmp_path, caplog):
    parser = argparse.ArgumentParser(parents=[rna.parsing.LogParser()])
    path = tmp_path / "record.log"
    format = "%(message)s"
    args, _ = parser.parse_known_args(
        f"--log_level 0 --log_file {str(path)} --log_format {format}".split()
    )
    assert args.log_level == 0

    logger = logging.getLogger(__name__)
    assert logger.level == 0

    logger.debug("Message")
    assert caplog.record_tuples == [("tests.test_parsing", 10, "Message")]

    log_text = path.read_text()
    assert "\x1b[38;5;249mMessage\x1b[0m\n" in log_text


def test_log_parser_default(tmp_path, caplog):
    parser = argparse.ArgumentParser(parents=[rna.parsing.LogParser(level=21)])
    args, _ = parser.parse_known_args([])
    logger = logging.getLogger()
    assert logger.level == 21
