# pylint:disable=missing-module-docstring
import logging
import rna.log


def calculate_stuff():
    logger = logging.getLogger("Other_module")
    # calculate something
    logger.critical("Critical error")


def test_colorlog(caplog):
    """
    Test if logger outputs expected messages
    """
    logger = (
        logging.getLogger()
    )  # This is the root logger. Every other logger derives from this
    logger.setLevel(logging.DEBUG)

    formatter = rna.log.ColorFormatter(
        "%(asctime)s - LoggingDemo:%(lineno)d - %(name)s - %(levelname)s: %(message)s"
    )

    terminal_handler = logging.StreamHandler()
    terminal_handler.setLevel(logging.WARNING)

    file_handler = logging.FileHandler(filename="./records.log", mode="w")
    file_handler.setLevel(logging.DEBUG)

    terminal_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)

    logger.addHandler(terminal_handler)
    logger.addHandler(file_handler)

    calculate_stuff()
    assert caplog.record_tuples == [("Other_module", 50, "Critical error")]


def test_termianl_handler(caplog):
    """
    Test if logger outputs expected messages
    """
    logger = logging.getLogger()
    logger.addHandler(rna.log.TerminalHandler())
    logger.info("Works")
    assert caplog.record_tuples == [("root", 20, "Works")]
