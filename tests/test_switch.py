# pylint:disable=missing-module-docstring,missing-class-docstring,missing-function-docstring
import unittest
from rna.pattern.switch import Switch


# pylint:disable=invalid-name
class distribute(Switch):
    """
    Global switch for the use of distributed computing
    """

    ENABLED = False


# pylint:disable=invalid-name
class stateful(Switch):
    """
    Global switch for the use of graph wrappers
    """


class TestGlobalSwitch(unittest.TestCase):
    def setUp(self):
        self.original = distribute.enabled()
        distribute.enable()

    def tearDown(self):
        distribute.ENABLED = self.original

    def test_enabled_basics(self):
        self.assertTrue(distribute.enabled())
        distribute.disable()
        self.assertFalse(distribute.enabled())
        distribute.enable()
        self.assertTrue(distribute.enabled())

    def test_enter(self):
        stateful.disable()
        with stateful():
            self.assertTrue(stateful.enabled())
        self.assertFalse(stateful.enabled())
        with stateful(True):
            self.assertTrue(stateful.enabled())
        stateful.enable()
        with stateful(False):
            self.assertFalse(stateful.enabled())

        with distribute(False), stateful(False):
            self.assertFalse(distribute.enabled())
            self.assertFalse(stateful.enabled())
            with distribute(True), stateful(True):
                self.assertTrue(distribute.enabled())
                self.assertTrue(stateful.enabled())
                with distribute(False), stateful(False):
                    self.assertFalse(distribute.enabled())
                    self.assertFalse(stateful.enabled())
                    with distribute(True), stateful(False):
                        self.assertTrue(distribute.enabled())
                        self.assertFalse(stateful.enabled())
                        with distribute(False), stateful(True):
                            self.assertFalse(distribute.enabled())
                            self.assertTrue(stateful.enabled())
                        self.assertTrue(distribute.enabled())
                        self.assertFalse(stateful.enabled())
                    self.assertFalse(distribute.enabled())
                    self.assertFalse(stateful.enabled())
                self.assertTrue(distribute.enabled())
                self.assertTrue(stateful.enabled())
            self.assertFalse(distribute.enabled())
            self.assertFalse(stateful.enabled())
