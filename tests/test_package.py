#!/usr/bin/env python

"""Tests for `rna` package."""

import unittest
import rna


class TestPackage(unittest.TestCase):
    """Tests for `rna` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""

        self.assertIsInstance(rna.__version__, str)
