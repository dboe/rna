Style Guide
-----------

Please follow the `google style guide <https://google.github.io/styleguide/pyguide.html>`_ illustrated
by `this example <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>`_.

Additionaly we format `*args` and `**kwargs` like so:

    .. code-block:: python

        def fun(param1: int, param2: float, *args, kw1: str = None, **kwargs)
            """
            Args:
                param1 (int): The first parameter.
                param2 (float): description
                *args (str): description
                    Second line. List follow now
                
                    0: first position is used as ...
                    1: second position is used as ...
                    
                    further are passed to/consumed by/used for ...

                kw1 (Optional[str]): description. Defaults to None.
                    Second line of description should be indented.
                **kwargs: description

                    **sub_key1 (Optional[str]): This key is retrieved by kwargs.pop('sub_key1', Default)
                    **sub_key2 (Optional[str]): This key is retrieved by kwargs.pop('sub_key1', Default)
                    
                    further are passed to/consumed by/used for ...

                ...
            """

TODO Notes
^^^^^^^^^^

If you add a `TODO` note to the code, please follow these conventions:

- Use the format `TODO(@responsible-user): note` to assign responsibility for the note.
- Place the `TODO` note in a docstring near the relevant code.
- Use priority labels like `TODO-0`, `TODO-1`, and `TODO-2` to indicate the urgency of the note.

We appreciate your contributions to the project and look forward to working with you!
