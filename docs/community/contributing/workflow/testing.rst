Testing
-------

To run tests, use

    .. code-block:: shell

        make test

To run a subset of tests, you have the following options:

- You can run a subset of tests by specifying a test file or test name

    .. code-block:: shell

        pytest tests/test_package.py

        pytest tests/test_package.py::Test_rna::test_version_type
    
- To test doctests, in docstrings or documentation, use the flag `--doctest-modules` (active by default via pyproject.toml)

    .. code-block:: shell

        pytest --doctest-modules docs/usage.rst

        pytest --doctest-modules rna/core.py -k "MyClass.function_with_doctest"

- Use the `--trace` option to directly jump into a pdb debugger on fails.

To run tests with code coverage, use the command

    .. code-block:: shell

        make coverage

This will generate an HTML report showing the coverage of each file in the project.
