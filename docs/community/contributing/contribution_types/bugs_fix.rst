Fixing Bugs
~~~~~~~~~~~

If you would like to help fix a bug in the project, take a look at the issues tagged as "bug" and "help wanted" in the project's issue tracker.
These issues are open to anyone who wants to help fix them.
