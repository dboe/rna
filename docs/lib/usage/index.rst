=====
Usage
=====
..
    How to use? Give me a primer.
    [usage]
    
..
    [essence-start]

To use rna in a project use

.. code-block:: python
   
   import rna
    
..
    [essence-end]
