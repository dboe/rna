library documentation
=====================

..
    [shields-start]

.. pypi
.. image:: https://img.shields.io/pypi/v/rna.svg
    :target: https://pypi.python.org/pypi/rna
.. ci
.. image:: https://gitlab.mpcdf.mpg.de/dboe/rna/badges/master/pipeline.svg
    :target: https://gitlab.mpcdf.mpg.de/dboe/rna/-/pipelines/latest

.. latest release
.. image:: https://gitlab.mpcdf.mpg.de/dboe/rna/-/badges/release.svg
    :target: https://gitlab.mpcdf.mpg.de/dboe/rna/-/releases

.. coverage
.. image:: https://gitlab.mpcdf.mpg.de/dboe/rna/badges/master/coverage.svg
    :target: https://gitlab.mpcdf.mpg.de/dboe/rna/commits/master

.. docs
.. image:: https://img.shields.io/website-up-down-green-red/https/dboe.pages.mpcdf.de/rna.svg?label=docs
    :target: https://dboe.pages.mpcdf.de/rna

.. pre-commit
.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit

..
    [shields-end]

Overview
--------

..
    [overview-start]

Basic and essential code building blocks of all pythons

..
    [overview-end]

Licensed under the ``MIT License``

Resources
~~~~~~~~~

..
    [resources-start]

* Source code: https://gitlab.mpcdf.mpg.de/dboe/rna
* Documentation: https://dboe.pages.mpcdf.de/rna
* Pypi: https://pypi.python.org/pypi/rna

..
    [resources-end]


Features
~~~~~~~~

..
    [features-start]

The following features should be highlighted:

* TODO

..
    [features-end]
