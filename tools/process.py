import shlex
import subprocess


def run_shell_command(command_line, *args, **kwargs):
    command_line_args = shlex.split(command_line)
    print(f"Running command '{command_line}'")
    subprocess.call(command_line_args, *args, **kwargs)
