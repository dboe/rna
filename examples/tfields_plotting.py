import rna
import pytest


def test_tfields_plotting_example():
    # Build a cube
    tfields = pytest.importorskip("tfields")
    cube = tfields.Mesh3D.grid((-1, 1, 2), (-1, 1, 2), (-5, -3, 2))
    cube.fields = [
        tfields.Tensors(list(range(len(cube)))),  # scalar
        tfields.Tensors(cube),  # 3D vector
    ]
    cube.maps[3].fields = [
        tfields.Tensors(list(range(len(cube.maps[3])))),  # scalar
        cube.triangles().norms(),  # norm vectors
    ]

    backends = ("matplotlib", "pyqtgraph")

    # plotting mesh in 3d with color coded faces:
    for backend in backends:
        rna.plotting.use(backend)
        cube.plot(dim=3, edgecolor="b", map_index=0)
        rna.plotting.show()

    # 2d mesh plot also available in matplotlib
    rna.plotting.use(backends[0])
    cube.plot(dim=2, edgecolor="r", map_index=0)
    rna.plotting.show()

    rna.plotting.use(backends[0])
    tfields.TensorFields(cube).plot(dim=2, field_index=1)
    tfields.TensorFields(cube).plot(dim=2)
    rna.plotting.show()

    tfields.TensorFields(cube).plot(dim=3, field_index=1)
    tfields.TensorFields(cube).plot(dim=3)
    rna.plotting.show()


if __name__ == "__main__":
    test_tfields_plotting_example()
