import logging
import rna
import rna.plotting
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO)
# rna.plotting.set_style("rna")


# Direct input
plt.rcParams["text.latex.preamble"] += r"\usepackage{lmodern}"
# Options
params = {
    "font.size": int(12),
    "font.family": "lmodern",
    "text.usetex": True,
}

plt.rcParams.update(params)
try:
    plt.rcParams.update(
        {
            "text.latex.unicode": True,
        }
    )
except KeyError:
    pass  # mpl >= 2.2

x = range(0, 10)
y = [t**2 for t in x]
z = [t**2 + 1 for t in x]
axes = rna.plotting.add_subplot()
artists = []
artists.append(axes.plot(x, y, label=r"$\beta=\alpha^2$"))
artists.append(axes.plot(x, z, label=r"$\beta=\alpha^2+1$"))
rna.plotting.set_labels(axes, r"HERE $\alpha$", r"$\beta$")
rna.plotting.set_legend(axes, *artists, loc=0)

latex_header = r"""
\documentclass[%ipt]{article}
\usepackage{pgf}
\begin{document}
\typeout{\the\textwidth}
HERE come the Figures:
""" % (
    plt.rcParams["font.size"]
)

latex_foot = r"""
\end{document}
"""

latex_figure = r"""
\begin{figure}
    \centering
    \resizebox{%spt}{!}{\input{%s}}
    \caption{The character size of the figure texts (lables etc) should be the same as HERE, 12345}
\end{figure}
"""

latex_document = latex_header

for width in [fraction * 390 for fraction in [1 / 4, 1 / 3, 1 / 2, 2 / 3, 1]]:
    # width in pt
    pgf_path = f"tex_figure_size_{width}.pgf"
    rna.plotting.save(pgf_path, fig=axes.figure, width=width, ratio=None, scale=1.0)

    latex_document += latex_figure % (width, pgf_path)

latex_document += latex_foot


tex_path = "rna_example_tex_figure_size.tex"
with open(tex_path, "w") as tex_file:
    tex_file.write(latex_document)

rna.process.execute(f"pdflatex {tex_path}")
