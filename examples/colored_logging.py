import logging
import rna.log

logger = logging.getLogger()
logger.setLevel(0)
logger.addHandler(rna.log.TerminalHandler())
logger.info("Info")
logger.warning("Warning")
logger.error("Error")
